import axios from 'axios';
import {API_CONF} from '~/api/ifinik-api20';
import Vue from 'vue';


class Api20 {
    constructor(store, app) {
        this.token=undefined;
        this.axios = axios.create({
            baseURL: API_CONF.url
        });
        this.app = app;
        this.store = store;
        app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
            this.session();
        }
    }
    async request(method, url, params, conf={}) {
        let _conf = { ...conf };
        _conf.headers = _conf.headers || {};
        if(! _conf.headers["Content-Type"]) {
            _conf.headers["Content-Type"] = "application/json";
        }
        if(conf.auth) {
            _conf.withCredentials = true;
            delete _conf.auth;
        }
        if(this.session_req) await this.session_req;
        if(this.token) _conf.headers.Token = this.token;
        else params._lang = this.app.i18n.locale; 
        return this.axios.request({
            url, method, params, ..._conf
        }).then((res) => {
            let data = res.data;
            if(conf.auth && data.token) this.setToken(data);
            if(data._need_token_refresh) this.session();
            return res.data;
        })
    }
    async get(url, params, conf)    { return this.request('get', url, params, conf) }
    async post(url, params, conf) {
        let data = JSON.stringify(params||{});
        let _conf = { ...(conf||{}), data: data };
        return this.request('post', url, {}, _conf);
    }
    async upload(url, params, file, conf) {
        let _conf = { ...(conf||{}), data: file };
        _conf.headers = {"Content-Type": "multipart/form-data"};
        url = '/upload' + url;
        return this.request('post', url, params, _conf);
    }
    async delete(url, params, conf) { return this.request('delete', url, params, conf) }
    async session() {
        if(! process.client) return false;
        let r = this.session_req = this.request('get', '/auth/session', {type: 'web', lang: this.app.i18n.locale}, {auth: true})
        .then( data => {this.session_req = null} )
        .catch( data => {this.session_req = null} );
        return r;
    }
    setToken(data) {
        this.token = data.token;
        if(data.token_refresh) {
            if(this.token_refresh) clearInterval(this.token_refresh);
            let timeout = data.token_refresh-10;
            if(timeout < 0) timeout=0.01;
            this.token_refresh = setTimeout(() => {
                this.token_refresh = undefined;
                this.session();
            }, timeout * 1000);
        }
        if('authorized' in data) {
            this.store.commit("modules/auth/changeStatusLogin", data.authorized);
        }
    }
}

export default async ({store, app}) => {
    store.$api = app.$api = new Api20(store, app);
    if(process.client) {
        Vue.prototype.$api = app.$api;
        app.$api.session();
    }
};
