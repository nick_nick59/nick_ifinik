import Vue from "vue";
import VueTimeago from "vue-timeago";

Vue.use(VueTimeago, {
  name: "timeago",
  locale: "en",
  converterOptions: {
    includeSeconds: false,
    addSuffix: false
  },
  locales: {
    ru: require("vue-timeago/node_modules/date-fns/locale/ru"),
    en: require("vue-timeago/node_modules/date-fns/locale/en")
  }
});
