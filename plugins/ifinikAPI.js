import axios from 'axios';

export default async ({store}) => {
  await axios.post(`https://api.ifinik.com/api/auth`)
    .then((res) => {
      store.commit('modules/news/setToken', res.data);
    })
    .catch(() => {
    });
  await store.commit('modules/news/loadingStarted');
  await store.dispatch('modules/news/loadNews');
};
