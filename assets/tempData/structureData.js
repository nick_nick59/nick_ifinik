export const data = {
    data: [
        {
            name: 'The Vanguard Group, Inc',
            num: '52'
        },
        {
            name: 'Berkshire Hathaway',
            num: '38'
        },
        {
            name: 'Blackrock, Inc',
            num: '4'
        },
        {
            name: 'State Street Corporation',
            num: '6'
        }
    ]
}
