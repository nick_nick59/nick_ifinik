export const data = {
  tables: [
    {
      chart: true,
      dataOnChart: true,
      columns: {
        title: 'Income statement',
        quarter1: {
          title: '1Q 2019'
        },
        quarter2: {
          title: '2Q 2019'
        },
        quarter3: {
          title: '3Q 2019'
        },
        quarter4: {
          title: '4Q 2019'
        }
      },
      data: [
        {
          title: 'Total Revenue',
          quarter1: ['207,906', '-11.6%'],
          quarter2: ['187,943', '+18.9%'],
          quarter3: ['257,324', '+11.2%'],
          quarter4: ['281,833']
        },
        {
          title: 'Net Income',
          quarter1: ['57,324', '-1.2%'],
          quarter2: ['55,835', '+6.32%'],
          quarter3: ['67,904', '+3.1%'],
          quarter4: ['69,003']
        },
        {
          title: 'EBITDA',
          quarter1: ['187,943', '-1.1%'],
          quarter2: ['187,943', '+1.02%'],
          quarter3: ['190,032', '+0.2%'],
          quarter4: ['187,994'],
          items: {
            data: [
              {
                title: 'Net Income Before Taxes',
                quarter1: ['87,908', '-0.01%'],
                quarter2: ['89,943', '+0.98%'],
                quarter3: ['94,741', '+1.9%'],
                quarter4: ['85,073'],
              },
              {
                title: 'Depreciation / Amortization',
                quarter1: ['98,083', '-0.6%'],
                quarter2: ['93,943', '+2.6%'],
                quarter3: ['87,987', '+2.5%'],
                quarter4: ['90,178'],
              },
              {
                title: 'Interest Expenses',
                quarter1: ['98,083', '-0.6%'],
                quarter2: ['93,943', '+2.6%'],
                quarter3: ['87,987', '+2.5%'],
                quarter4: ['90,178'],
              }
            ]
          }
        }
      ]
    },
    
    {
      chart: true,
      dataOnChart: false,
      columns: {
        title: 'Cash Flow',
        quarter1: {
          title: '1Q 2019'
        },
        quarter2: {
          title: '2Q 2019'
        },
        quarter3: {
          title: '3Q 2019'
        },
        quarter4: {
          title: '4Q 2019'
        }
      },
      data: [
        {
          title: 'Net Cash from Operating Activities',
          quarter1: ['205,266', '-1.1%'],
          quarter2: ['204,906', '+1.8%'],
          quarter3: ['203,086', '+0.2%'],
          quarter4: ['207,988']
        },
        {
          title: 'Cash at the End of the Period',
          quarter1: ['205,266', '-1.1%'],
          quarter2: ['204,906', '+1.8%'],
          quarter3: ['203,086', '+0.2%'],
          quarter4: ['207,988']
        }
      ]
    },
    
    {
      chart: true,
      dataOnChart: false,
      columns: {
        title: 'Balance Sheet',
        quarter1: {
          title: '1Q 2019',
          file: '/test.pdf'
        },
        quarter2: {
          title: '2Q 2019',
          file: '/test.pdf'
        },
        quarter3: {
          title: '3Q 2019',
          file: '/test.pdf'
        },
        quarter4: {
          title: '4Q 2019',
          file: '/test.pdf'
        }
      },
      data: [
        {
          title: 'Total Assets',
          quarter1: ['187,943', '-1.1%'],
          quarter2: ['187,943', '+1.02%'],
          quarter3: ['190,032', '+0.2%'],
          quarter4: ['187,994'],
          items: {
            data: [
              {
                title: 'Current Assets',
                quarter1: ['87,908', '-0.01%'],
                quarter2: ['89,943', '+0.98%'],
                quarter3: ['94,741', '+1.9%'],
                quarter4: ['85,073'],
              },
              {
                title: 'Non-current Assets',
                quarter1: ['87,908', '-0.01%'],
                quarter2: ['89,943', '+0.98%'],
                quarter3: ['94,741', '+1.9%'],
                quarter4: ['85,073'],
              }
            ]
          }
        },
        {
          title: 'Total Liabilities',
          quarter1: ['1,782,043', '-1.3%'],
          quarter2: ['1,087,943', '+0.9%'],
          quarter3: ['1,787,908', '-1.5%'],
          quarter4: ['1,287,033'],
          items: {
            data: [
              {
                title: 'Long Term Debt',
                quarter1: ['98,083', '-0.6%'],
                quarter2: ['93,943', '+2.6%'],
                quarter3: ['87,987', '+2.5%'],
                quarter4: ['90,178'],
              },
              {
                title: 'Short Term Debt',
                quarter1: ['98,083', '-0.6%'],
                quarter2: ['93,943', '+2.6%'],
                quarter3: ['87,987', '+2.5%'],
                quarter4: ['90,178'],
              },
              {
                title: 'Other Liabilities',
                quarter1: ['98,083', '-0.6%'],
                quarter2: ['93,943', '+2.6%'],
                quarter3: ['87,987', '+2.5%'],
                quarter4: ['90,178'],
              }
            ]
          }
        },
        {
          title: 'Total Equity',
          quarter1: ['1,782,043', '-1.3%'],
          quarter2: ['1,087,943', '+0.9%'],
          quarter3: ['1,787,908', '-1.5%'],
          quarter4: ['1,287,033']
        }
      ]
    },
  ],
  
  tableRatios: {
    columns: {
      title: 'Ratios',
      quarter1: {
        title: '1Q 2019'
      },
      quarter2: {
        title: '2Q 2019'
      },
      quarter3: {
        title: '3Q 2019'
      },
      quarter4: {
        title: '4Q 2019'
      }
    },
    data: [
      {
        title: 'Debt / EBITDA',
        hint: '(Long Term Debt + Short Term Debt)  / EBITDA',
        quarter1: ['0.5', '0'],
        quarter2: ['0.5', '0'],
        quarter3: ['0.5', '0'],
        quarter4: ['0.5']
      },
      {
        title: 'Net Debt / EBITDA',
        hint: '(Long Term Debt + Short Term Debt)  / EBITDA',
        quarter1: ['0.1', '+1.6%'],
        quarter2: ['0.2', '0'],
        quarter3: ['0.2', '0'],
        quarter4: ['0.2']
      },
      {
        title: 'P/S (Price to Sales) <span class="text-disabled">TTM</span>',
        quarter1: ['10.1', '-1.1%'],
        quarter2: ['9.87', '+1.8%'],
        quarter3: ['10.3', '+0.2%'],
        quarter4: ['10.5']
      },
      {
        title: 'P/E (Price to Earnings) <span class="text-disabled">TTM</span>',
        quarter1: ['32.88', '-1.1%'],
        quarter2: ['31.23', '+1.8%'],
        quarter3: ['32.4', '+0.2%'],
        quarter4: ['32.61']
      },
      {
        title: 'EV / EBITDA',
        quarter1: ['0.1', '+1.6%'],
        quarter2: ['0.2', '0'],
        quarter3: ['0.2', '0'],
        quarter4: ['0.2']
      }
    ]
  },
  
  tableCustom: {
    columns: {
      title: 'Custom Ratios & multipliers',
      quarter1: {
        title: '1Q 2019'
      },
      quarter2: {
        title: '2Q 2019'
      },
      quarter3: {
        title: '3Q 2019'
      },
      quarter4: {
        title: '4Q 2019'
      }
    },
    data: [
      {
        title: 'Ratio 1',
        quarter1: ['0.1', '+1.6%'],
        quarter2: ['0.2', '0'],
        quarter3: ['0.2', '0'],
        quarter4: ['0.2']
      },
      {
        title: 'Ratio 2',
        quarter1: ['0.5', '0'],
        quarter2: ['0.5', '0'],
        quarter3: ['0.5', '0'],
        quarter4: ['0.5']
      },
      {
        title: 'Ratio 3',
        quarter1: ['0.1', '+1.6%'],
        quarter2: ['0.2', '0'],
        quarter3: ['0.2', '0'],
        quarter4: ['0.2']
      },
    ]
  }
};
