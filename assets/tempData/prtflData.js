export const data = {
    cnsPE: '8.4',
    cnsGrw: '+36%',
    epsGrw: '2.8',
    dvdYield: '4%',
    vlt: '14.3%',
    betaTrend: '1.8'
}