export const data = {
    'Previous close': '199.98',
    'Open': '201.98',
    'Day’s range': '198.22 - 208.54',
    '52W Range': '101.73 - 222.46',
    'Market Cap': '245B',
    'Volume': '3’465’578',
    'P/E ratio (TTM)': '15.62',
    'Forward Dividend': '4.21',
    'Ex-Dividend Date': '2019-12-25',
    'Dividend Yield': '6.7%'
}
