import axios from "axios";
import { toKebabCase } from '../components/News/createNewsID';


export const newsRoutes = async (routes) => {
  let userToken;
  await axios
    .post(`https://api.ifinik.com/api/auth`)
    .then(res => {
      userToken = res.data;
    })
    .catch(() => {});

   await axios.get(`https://api.ifinik.com/api/published-news`, {
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
          'Cache-Control' : 'no-cache',
          'X-User-Token': userToken,
        }
      })
        .then((res) => {
          const items = res.data;
          items.forEach(el => {
            const id = el.id + '-' + toKebabCase(el.title);
            routes.push('/news/feed/' + id)
            routes.push('/ru/news/feed/' + id)
          });
        })
        .catch(() => {
        });
        console.log(routes);
  return routes;
};
