import modal from './modules/modal';
import auth from './modules/auth';
import news from './modules/news';
import screener from './modules/screener';
import links from './modules/links';


export default {
  modules: {
    modal,
    auth,
    news,
    screener,
    links
  },
};
