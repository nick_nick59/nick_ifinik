import { toKebabCase } from '../../components/News/createNewsID';
import axios from "axios";

export default {
  state: () => ({
    apiToken: null,
    news: null,
    numPage: 1,
    isLoading: false,
  }),
  mutations: {
    setToken(state, token) {
      state.apiToken = token;
    },
    doNextPage(state) {
      state.numPage += 1;
    },
    loadingStarted(state) {
      state.isLoading = true;
    },
    createNews(state, items) {
      items.forEach(el => {
        const id = el.id + '-' + toKebabCase(el.title);
        state.news = {
          ...state.news,
          [id]: {
            id: id,
            title: el.title,
            description: el.text,
            date: el.date,
            source: el.source,
            photos: el.photos,
            thumbs: el.thumbs,
            companies: el.newCompanies,
          }
        };
      });
      state.isLoading = false;
    }
  },
  actions: {
    loadNews({state, commit}) {
      axios.get(`https://api.ifinik.com/api/news?page=${state.numPage}`, {
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
          'Cache-Control' : 'no-cache',
          'X-User-Token': state.apiToken,
        }
      })
        .then((res) => {
          commit('createNews', res.data);
        })
        .catch(() => {
        });
    }
  }
};
