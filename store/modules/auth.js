export default {
  actions: {
    async login({ dispatch, commit }, data) {
      await this.$api
        .post(
          "/auth/login",
          {
            acc_type: data.acc_type,
            login: data.login,
            pass: data.pass,
            code: data.code
          },
          { auth: true }
        )
        .then(data => {
          this.dispatch("modules/modal/changePopup", false);
          dispatch("getCurrentUser");
          if (data.acc_type === "phone") {
            commit("changeSendingCodeStatus", {
              type: data.acc_type,
              status: false
            });
          }
        })
        .catch(err => {
          if (data.acc_type === "phone" && err.response.data.code === 401) {
            commit("changeSendingCodeStatus", {
              type: data.acc_type,
              status: true
            });
          }
          commit("setErrorCode", `${err.response.data.code}`);
        });
    },
    menuState({ commit }, param) {
      commit("changeMenuState", param);
    },
    async logout({ commit, state }) {
      await this.$api
        .post("/auth/logout", {}, { auth: true })
        .then(() => {
          commit("setUser", "");
        })
        .catch(err => {});
    },
    async register({ dispatch, commit, state }, data) {
      await this.$api
        .post(
          "/auth/register",
          {
            acc_type: data.acc_type,
            login: data.login,
            pass: data.pass,
            code: data.code
          },
          { auth: true }
        )
        .then(res => {
          this.dispatch("modules/modal/changePopup", false);
          commit("changeSendingCodeStatus", {
            type: data.acc_type,
            status: false
          });
        })
        .catch(err => {
          console.log(err.response);
          if (err.response.status === 401) {
            commit("changeSendingCodeStatus", {
              type: data.acc_type,
              status: true
            });
          }
          if (
            err.response.data.code === 403007 ||
            err.response.data.code === 403006
          ) {
            commit("setErrorCode", `${err.response.data.code}`);
          }
        });
    },
    async resetPassword({ commit, state }, email) {
      await this.$api
        .post(
          "/auth/recover",
          {
            acc_type: "email",
            login: email
          },
          { auth: true }
        )
        .then(res => {})
        .catch(err => {
          if (err.response.data.code === 401) {
            commit("changeSendingCodeStatus", {
              type: "email",
              status: true
            });
          }
        });
    },
    async changePassword({ commit, state }, data) {
      await this.$api
        .post(
          "/auth/password",
          {
            acc_type: "email",
            login: data.login,
            pass: data.pass,
            code: data.code
          },
          { auth: true }
        )
        .then(res => {
          this.dispatch("modules/modal/changePopup", false);
          commit("changeSendingCodeStatus", {
            type: "email",
            status: false
          });
        })
        .catch(err => {});
    },
    showErrors({ commit }, error) {
      commit("setError", error);
    },
    async getCurrentUser({ commit }) {
      this.$api.get("/auth/user", {}, { auth: true }).then(res => {
        console.log(res);
        commit("setUser", res);
      });
    }
  },
  mutations: {
    setError(state, error) {
      state.error = error;
    },
    setErrorCode(state, code) {
      state.authErrCode = code;
    },
    clearError(state) {
      state.error = null;
    },
    changeStatusLogin(state, param) {
      state.loginStatus = param;
    },
    resetPswd(state) {
      state.resetPassword = true;
      setTimeout(() => {
        this.dispatch("modules/modal/changePopup", false);
      }, 1500);
    },
    setUser(state, repsonse) {
      state.currentUser = repsonse;
    },
    changeMenuState(state, param) {
      state.menuStatus = param;
    },
    changeSendingCodeStatus(state, { type, status }) {
      if (type === "email") {
        state.isSendingEmailCode = status;
      } else if (type === "phone") {
        state.isSendingSmsCode = status;
      }
    }
  },
  state: () => ({
    error: null,
    authErrCode: null,
    loginStatus: false,
    resetPassword: false,
    currentUser: "",
    menuStatus: "",
    isSendingEmailCode: false,
    isSendingSmsCode: false
  }),
  getters: {
    error: s => s.error
  }
};
