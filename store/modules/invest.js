import { createRandomID } from "~/components/Screener/randomID";
export default {
  state: () => ({
    currentScreener: "default-screener",
    stocksArr: [],
    nextToken: null,
    isLoading: false,
    stocksCount: null,
    columns: [
      {
        metric: "tiker",
        title: "prtflPage.symbol",
        isActive: false,
        isIconLeft: false,
        sortKind: "name",
        class: "table-col__two gray brdr-right"
      },
      /*{
        metric: "name",
        title: "prtflPage.company",
        isActive: true,
        isIconLeft: true,
        sortKind: "name",
        class: "table-col__three gray brdr-right"
      },*/
      {
        metric: "vl_def",
        title: "",
        isActive: false,
        isIconLeft: true,
        sortKind: "value",
        class: "table-col__four gray price-column brdr-right"
      },
      {
        metric: "gty",
        title: "prtflPage.gty",
        isActive: false,
        isIconLeft: true,
        sortKind: "value",
        class: "table-col__four gray price-column brdr-right"
      },
      {
        metric: "last_price",
        title: "prtflPage.last_price",
        isActive: false,
        isIconLeft: true,
        sortKind: "value",
        class: "table-col__four gray price-column brdr-right"
      },
      
      {
        metric: "mkt_value",
        title: "prtflPage.mkt_value",
        isActive: false,
        isIconLeft: true,
        sortKind: "value",
        class: "table-col__four gray brdr-right"
      },
      {
        metric: "total_gain",
        title: "prtflPage.total_gain",
        isActive: false,
        isIconLeft: true,
        sortKind: "value",
        class: "table-col__four gray"
      }
    ],
    savedScreeners: {
      "savedScreener-topGainers": {
        id: "savedScreener-topGainers",
        name: "stocksPage.topGainers",
        translatable: true,
        sort: {
          metric: "ratio",
          ascending: false,
          active: true,
          sortKind: "value"
        },
        selectedFilters: {}
      },
      "savedScreener-topLoosers": {
        id: "savedScreener-topLoosers",
        name: "stocksPage.topLoosers",
        translatable: true,
        sort: {
          metric: "ratio",
          ascending: true,
          active: true,
          sortKind: "value"
        },
        selectedFilters: {}
      },
      "default-screener": {
        id: "default-screener",
        name: "",
        sort: {
          sortKind: "name",
          metric: "name",
          active: true,
          ascending: true
        },
        selectedFilters: {}
      }
    }
  }),
  mutations: {
    addFilter(state, filter) {
      const current = state.savedScreeners[state.currentScreener];
      if (current.selectedFilters[filter.id]) {
        return;
      } else {
        current.selectedFilters = {
          ...current.selectedFilters,
          [filter.id]: filter
        };
      }
    },
    deleteFilter(state, filter) {
      const current = state.savedScreeners[state.currentScreener];
      current.selectedFilters[filter.id].isAdded = false;
      const selectedFilters = { ...current.selectedFilters };
      delete selectedFilters[filter.id];
      current.selectedFilters = { ...selectedFilters };
    },
    addOption(state, { filterID, optionID }) {
      const current = state.savedScreeners[state.currentScreener];
      current.selectedFilters[filterID].options[optionID].isAdded = true;
    },
    deleteOption(state, { filterID, optionID }) {
      const current = state.savedScreeners[state.currentScreener];
      current.selectedFilters[filterID].options[optionID].isAdded = false;
    },
    /**** Dropdown filter ****/

    selectOption(state, { filterID, option }) {
      const current = state.savedScreeners[state.currentScreener];
      current.selectedFilters[filterID].selectedOption = option;
    },

    selectScreener(state, id) {
      state.currentScreener = id;
    },
    saveScreener(state, name) {
      const newID = createRandomID();
      const current = state.savedScreeners[state.currentScreener];
      state.currentScreener = newID;
      state.savedScreeners = {
        ...state.savedScreeners,
        [newID]: {
          id: newID,
          name: name,
          sort: {
            sortKind: "name",
            metric: "full_name",
            active: true,
            ascending: true
          },
          selectedFilters: current.selectedFilters
        }
      };
      state.currentScreener = newID;
    },
    changeSort(state, { metric, ascending, active, sortKind }) {
      const current = state.savedScreeners[state.currentScreener];
      current.sort.active = active;
      current.sort.metric = metric;
      current.sort.ascending = ascending;
      current.sort.sortKind = sortKind;
    },
    setNextToken(state, token) {
      state.nextToken = token;
    },
    setStocksCount(state, count) {
      state.stocksCount = count;
    },
    updateStocksArr(state, array) {
      state.stocksArr = [...state.stocksArr, ...array];
    },
    setNewStocksArr(state, array) {
      state.stocksArr = array;
    },
    switchLoadStatus(state, status) {
      state.isLoading = status;
    }
  },
  actions: {
    async loadFirstTwentyInstruments({commit, state}, param) {
      const currentID = state.currentScreener;
      const currentScreener = state.savedScreeners[currentID];
      const kindSort = currentScreener.sort.metric;
      let methodSort = currentScreener.sort.ascending ? 'asc' : 'desc';
      param.tblData ? commit('setNewStocksArr', param.tblData) :
      await this.$api.get("/stocklist", {order: kindSort + " " + methodSort})
          .then(data => {
            console.log(data);
            commit('setNextToken', data["_next"]);
            commit('setStocksCount', data.count);
            if (param.changeSorting === true) {
              commit('setNewStocksArr', data.data);
            } else {
              commit('updateStocksArr', data.data);
            }
          })
          .catch(e => {
          })
        },
    async loadNextCompanies({commit, state}) {
      if (state.nextToken) {
        commit('switchLoadStatus', true);
        await this.$api.get("/stocklist", {_next:  state.nextToken})
          .then(data => {
            commit('setNextToken', data["_next"]);
            commit('updateStocksArr', data.data);
          })
          .catch(e => {
          })
        commit('switchLoadStatus', false);
      }
    },
  }
  
};