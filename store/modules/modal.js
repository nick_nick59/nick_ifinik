export default {
    actions: {
        changePopup(ctx, param) {
            ctx.commit('changeModalState', param)
        }
    },
    mutations: {
        changeModalState: (state, param) => {
            state.modalState = param.show;
            state.formType = param.event;
        },
    },
    state: () => ({
        modalState: false,
        formType: ''

    }),
    getters: {
        getModalState(state) {
            return state.modalState;
        }
    }
}
