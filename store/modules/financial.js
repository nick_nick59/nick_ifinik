export default {
    mutations: {
        changeChartState: (state, data) => {
            state.financialChartState = data;
        },
        savePeriodState: (state, data) => {
            state.financialPeriodState = data;
        }
    },
    state: () => ({
        financialChartState: '',
        financialPeriodState: '',
    })
}
