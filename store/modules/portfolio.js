
var currentParam = 1;
var stateArr = []
export default {
    actions: {
        loadCompleteCompany(ctx, param) {
            ctx.commit('changeDataState', param)
        },
        loadCompleteMarket(ctx, param) {
            ctx.commit('changeDataStateMarket', param)
        },
        loadCompleteChart(ctx, param) {
            ctx.commit('changeDataStateChart', param)
        },
        getCompanyInfo({dispatch, commit}, param) {
            commit('companyState', param)
        },
    },
    mutations: {
        changeDataState: (state, param) => {
            state.dataStatus = param;
        },
        changeDataStateMarket: (state, param) => {
            state.dataStatusMarket = param;
        },
        changeDataStateChart: (state, param) => {
            state.dataStatusChart = param;
        },
        historyArrState: (state, param) => {
            state.dataHistory = param;
        },
        bannerState: (state, param) => {
            state.dataBanner = param;
        },
        photoState: (state, param) => {
            state.dataPhotoList = param;
        },
        companyState: (state, param) => {
            state.dataCompany = param;
        },
        pageTypeState: (state, param) => {
            state.pageTypeData = param;
        },
        realtimeState: (state, response) => {
            state.realtimeData = '';
            state.realtimeData = response;
        },
        realTimeParamArrayState: (state, response) => {
            state.realTimeParamArray = response;
        }
    },
    state: () => ({
        dataStatus: '',
        dataStatusMarket: '',
        dataStatusChart: '',
        dataHistory: '',
        dataBanner: '',
        dataPhotoList: '',
        dataCompany: '',
        pageTypeData: '',
        realtimeData: '',
        realTimeParamArray: []
    })
}