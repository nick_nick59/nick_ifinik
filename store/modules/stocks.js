export default {
  state: () => ({
    isLoadingData: false,
    activeSort: {
      sortKind: 'name',
      metric: 'full_name',
      active: true,
      ascending: true,
    },
    columns: ['ticker', 'full_name', 'price', 'change', 'changePerc'],
  }),
  mutations: {
    changeSorting(state, {metric, ascending, active, sortKind}) {
      state.activeSort.active = active;
      state.activeSort.metric = metric;
      state.activeSort.ascending = ascending;
      state.activeSort.sortKind = sortKind;
    },
    setSortGainers(state) {
      state.activeSort.active = true;
      state.activeSort.metric = 'changePerc';
      state.activeSort.ascending = true;
      state.activeSort.sortKind = 'value';
    },
    setSortLoosers(state) {
      state.activeSort.active = true;
      state.activeSort.metric = 'changePerc';
      state.activeSort.ascending = false;
      state.activeSort.sortKind = 'value';
    },
    finishLoading(state) {
      state.isLoadingData = false;
    },
    startLoading(state) {
      state.isLoadingData = true;
    },
  }

};
