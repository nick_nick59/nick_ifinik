import { I18N } from "./config/index.js";
const axios = require("axios");

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "iFinik.com",
    script: [
      {
        src: "https://apis.google.com/js/platform.js?onload=init",
        acync: true,
        defer: true
      },
      {
        src:
          "https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v8.0&appId=656908631686060&autoLogAppEvents=1",
        acync: true,
        defer: true,
        rossorigin: "anonymous",
        nonce: "rkTjknUo"
      }
    ],
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no"
      },
      {
        hid: "description",
        name: "description",
        content: "iFinik.com - инвестиции"
      },
      { name: "msapplication-TileColor", content: "#ffffff" },
      { name: "msapplication-TileImage", content: "/ms-icon-144x144.png" },
      { name: "yandex-verification", content: "ba70c9b5b2db7fa3" },
      {
        name: "google-site-verification",
        content: "bivz9hu0qjucxfM03s_H1uGra42bk0zxqqo4UPl_s_M"
      },
      { name: "google-signin-scope", content: "profile email" },
      {
        name: "google-signin-client_id",
        content:
          "813798596592-645fa2v6uu7m71cqk3902e9ciaqk629t.apps.googleusercontent.com"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "icon",
        type: "image/png",
        size: "16x16",
        href: "/favicon-16x16.png"
      },
      {
        rel: "icon",
        type: "image/png",
        size: "32x32",
        href: "/favicon-32x32.png"
      },
      {
        rel: "icon",
        type: "image/png",
        size: "96x96",
        href: "/favicon-96x96.png"
      },
      {
        rel: "icon",
        type: "image/png",
        size: "192x192",
        href: "/android-icon-192x192.png"
      },
      { rel: "apple-touch-icon", size: "57x57", href: "/apple-icon-57x57.png" },
      { rel: "apple-touch-icon", size: "60x60", href: "/apple-icon-60x60.png" },
      { rel: "apple-touch-icon", size: "72x72", href: "/apple-icon-72x72.png" },
      { rel: "apple-touch-icon", size: "76x76", href: "/apple-icon-76x76.png" },
      {
        rel: "apple-touch-icon",
        size: "114x114",
        href: "/apple-icon-114x114.png"
      },
      {
        rel: "apple-touch-icon",
        size: "120x120",
        href: "/apple-icon-120x120.png"
      },
      {
        rel: "apple-touch-icon",
        size: "144x144",
        href: "/apple-icon-144x144.png"
      },
      {
        rel: "apple-touch-icon",
        size: "152x152",
        href: "/apple-icon-152x152.png"
      },
      {
        rel: "apple-touch-icon",
        size: "180x180",
        href: "/apple-icon-180x180.png"
      },
      { rel: "apple-touch-icon", size: "57x57", href: "/apple-icon-57x57.png" }
    ]
  },
  css: ["@/assets/scss/main.scss"],
  modules: [
    ["nuxt-i18n", I18N],
    ["@nuxtjs/google-analytics", { id: "UA-146444514-3" }],
    "@nuxtjs/style-resources",
    "nuxt-svg-loader",
    "@nuxtjs/svg-sprite",
    "bootstrap-vue/nuxt",
    "vue-social-sharing/nuxt",
    "@nuxtjs/auth-next"
  ],
  svgSprite: {
    input: "~/assets/svg/",
    output: "~/assets/sprite/gen",
    defaultSprite: "icons",
    elementClass: "icon"
  },
  styleResources: {
    scss: ["~/assets/scss/base/vars.scss"]
  },
  plugins: [
    { src: "~/plugins/api20", rel: "preload" },
    { src: "~/plugins/validate", rel: "preload" },
    { src: "~/plugins/ifinikAPI", rel: "preload" },
    // { src: '~plugins/vue-chartjs.js', ssr: false, rel: 'preload' },
    { src: "~plugins/masonry.js", ssr: false, rel: "preload" },
    { src: "~plugins/infinite-scroll.js", ssr: false, rel: "preload" },
    { src: "~plugins/range-slider.js", ssr: false, rel: "preload" },
    { src: "~plugins/timeago.js", ssr: false, rel: "preload" },
    { src: "~plugins/vue-select.js", rel: "preload" },
    { src: "~plugins/finance.js", rel: "preload" },
    { src: "~/plugins/swiper.js", ssr: false, rel: "preload" }
  ],
  auth: {
    strategies: {
      google: {
        clientId: "..."
      },
      facebook: {
        endpoints: {
          userInfo:
            "https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email,birthday"
        },
        clientId: "...",
        scope: ["public_profile", "email", "user_birthday"]
      }
    }
  },
  vendor: ["axios"],
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  router: {
    middleware: "news-feed"
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    // extend (config, { isDev, isClient }) {
    //   if (isDev && isClient) {
    //     config.module.rules.push({
    //       enforce: 'pre',
    //       test: /\.(js|vue)$/,
    //       loader: 'eslint-loader',
    //       exclude: /(node_modules)/
    //     })
    //   }
    // }
  }
};
