<template>
  <div>
    <FullNews :rating="0" :selectedNews="itemNews" />
    <div class="stocks" v-if="Object.values(tickers)[0]">
      <div class="stocks__caption-container">
        <h3 class="stocks__caption">
          {{ $t("newsPage.mentionedCompanies") }}
        </h3>
      </div>
      <div class="stocks__table">
        <StocksTable :newsCompanies="itemNews.newCompanies" :logos="logos" :tickers="tickers"/>
      </div>
    </div>
  </div>
</template>

<script>
import FullNews from "~/components/News/FullNews.vue";
import { values } from "~/components/News/values-company";
import axios from "axios";
import { mapState } from "vuex";

import StocksTable from "~/components/News/StocksTable.vue";

export default {
  async asyncData({ store, params }) {
    const newsId = params.id.split("-")[0];

    let detailNews = await axios.get(
      `https://api.ifinik.com/api/one-news?id=` + newsId,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "*/*",
          "Access-Control-Allow-Origin": "*",
          "Cache-Control": "no-cache",
          "X-User-Token": store.state.modules.news.apiToken
        }
      }
    );

    let relatedNews;
    if (store.state.modules.news.news) {
      relatedNews = Object.values(store.state.modules.news.news);
    } else {
      var listNews = await axios.get(`https://api.ifinik.com/api/news?page=1`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "*/*",
          "Access-Control-Allow-Origin": "*",
          "Cache-Control": "no-cache",
          "X-User-Token": store.state.modules.news.apiToken
        }
      });
      relatedNews = listNews.data;
    }
    const tableValues = values;
    return {
      tableValues: tableValues,
      relatedNews: relatedNews,
      itemNews: detailNews.data
    };
  },
  name: "idNewsFeed",
  mounted() {
  },
  data() {
    return {
      tickers: {},
      logos: {},
      loadingData: false
    }
  },
  watch: {
    itemNews() {
    }
  },
  components: {
    FullNews,
    StocksTable
  },
  computed: {
    lang() {
      return this.$i18n.locale;
    },
  },
};
</script>

<style lang="scss" scoped>
.section-title {
  margin-bottom: ($spacer * 5);
  background: $white;
  border-radius: $border-radius;

  &__text {
    padding: ($spacer * 6);
    font-style: normal;
    font-weight: bold;
    font-size: $font-size-xl;
    color: $text-color;
  }
}
.stocks {
  background: $white;
  border-radius: $border-radius;
  margin-bottom: $spacer * 5;
  
  @media #{$tabletLg} {
    margin-bottom: $spacer * 4;
  }
  
  @media #{$mobile} {
    border-radius: 0;
    margin: 0 (-$spacer * 4) ($spacer * 4);
  }

  &__caption-container {
    box-shadow: inset 0px -1px 0px $primary-bg;
  }
  &__caption {
    padding: $spacer * 6;
  
    @media #{$mobile} {
      padding: $spacer * 4 $spacer * 6;
    }
  }
  &__table {
    padding: $spacer * 6;
  }
}
</style>
