export default function({ redirect, app, store, route, params, error }) {
  if (route.fullPath === "/news/feed/" || route.fullPath === "/ru/news/feed/") {
    const news = Object.values(store.state.modules.news.news);
    const firstNewsId = news[0].id;
    redirect(app.localePath(`/news/feed/${firstNewsId}`));
  }
}
