import en from '../assets/local/en-US.js'
import ru from '../assets/local/ru-RU.js'

export const I18N = {
    locales: [
        {
            code: 'en',
            iso: 'en-US',
            name: 'English'
        },
        {
            code: 'ru',
            iso: 'ru-Ru',
            name: 'Русский'
        }
    ],
    defaultLocale: 'en',
    detectBrowserLanguage: {
        useCookie: true,
        alwaysRedirect: true,
        cookieKey: 'lang'
    },
    vueI18n: {
        fallbackLocale: 'en',
        messages: {en, ru}
    }
}
