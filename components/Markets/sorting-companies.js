export const sortByName = (ascending, metric) => (a, b) => {
  let nameA;
  let nameB;
  if (metric === 'full_name') {
    nameA = a.fullName.toUpperCase();
    nameB = b.fullName.toUpperCase();
  } else if (metric === 'ticker') {
    nameA = a.ticker.toUpperCase();
    nameB = b.ticker.toUpperCase();
  }
  if (nameA < nameB) {
    return ascending ? -1 : 1;
  }
  if (nameA > nameB) {
    return ascending ? 1 : -1;
  }

  return 0;
};

export const sortByValues = (asc, metric) => (a, b) => {
  if (!asc) {
    return a[metric] - b[metric];
  } else {
    return b[metric] - a[metric];
  }
};